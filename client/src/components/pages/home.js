import React from "react";

import { UserViewer } from "./../prezzos/userviewer";

const Home = () => {

  let el = (
    <>
      <div className="container-fluid">
        <div className="row">
          
          <div className="mt-4 mx-auto card">
            <div className="card-body">
              <h2>Welcome&hellip; dude!</h2>
            </div>
          </div>
          
        </div>
      </div>

      <div className="container-fluid">

        <h3>View Users</h3>
        
        <div className="row">
          <UserViewer />
        </div>

      </div>
    </>
  );
  return el;
};

export { Home };
import React from "react";
import { useState } from "react";

import { UserViewer } from "./../prezzos/userviewer";
import { UserEditor } from "./../prezzos/usereditor";

const Admin = () => {

  const [ editing, setEditing ] = useState(null);  

  const resetEditorHandler = () => {
    setEditing( null ); 
  };
  
  const createNewUserHandler = () => {
    setEditing( {} ); 
  };
  
  const editExistingUserHandler = (user) => {
    setEditing( user ); 
  };

  let el = (
    <>
      <div className="container-fluid">
        <div className="row">
          
          <div className="mt-4 mx-auto card">
            
            <div className="card-body">
              <h2>Welcome back&hellip; dude!</h2>
            </div>
            
          </div>
          
        </div>
      </div>

      <div className="container-fluid">

        <h3>Edit Users</h3>

        { editing ? (<UserEditor user={ editing } onClose={ resetEditorHandler } />) : null }
        
        <div className="row">
          <button id="objectbutton" type="button" className="my-2 btn btn-primary btn-lg" onClick={ createNewUserHandler }>
            New User
          </button>
        </div>

        <div className="row">
          <UserViewer canEdit={ () => true } onEdit={ editExistingUserHandler } />
        </div>

      </div>
    </>
  );
  return el;
};

export { Admin };
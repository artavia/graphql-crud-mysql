import React from "react";

const { NODE_ENV } = process.env;

const AlphaTest = () => {
  
  let el = (
    <>
      <div className="container-fluid">

        <h1 className="display-3">AlphaTest!</h1>

        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">

              <div className="card mt-4">
                <div className="card-body">
                  
                  <small>You are running this application in <b>{NODE_ENV}</b> mode.</small>

                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </>
  );
  return el;
};

export { AlphaTest };
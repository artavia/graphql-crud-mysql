import React , { useState, useEffect, useRef, useCallback } from "react";

import { SharedModal } from "./../shared/SharedModal";
import { AddForm } from "./../forms/addform";
import { EditForm } from "./../forms/editform";

const UserEditor = ( props ) => {
  
  // console.log( "UserEditor props", props );

  let { user, onClose } = props;  
  // console.log( "user", user ); 
  
  let isMountedRef = useRef(null); 
  const [ showModal, setShowModal ] = useState( false ); 

  const leakSilencer = useCallback( () => {
    
    isMountedRef.current = true;

    if(isMountedRef.current){
      setShowModal(true);
    }
    return () => {
      wipeItOut();
    }
    
  } , [] );

  useEffect( () => {
    leakSilencer();
  } , [ leakSilencer ] );

  const parentStateHandler = () => { onClose(); };
  const initializeModalCloseHandler = () => { parentStateHandler(); };
  const finalizeUserHandler = () => { initializeModalCloseHandler(); }; 

  const wipeItOut = () => { 
    setShowModal(false); 
    isMountedRef.current = false;
  };

  let el = (
    <SharedModal finalizeUserHandler={ finalizeUserHandler } user={user}>
      
      {
        user.id 

        ? (<EditForm finalizeUserHandler={ finalizeUserHandler } user={user} />) 

        : (<AddForm />)
      }

    </SharedModal>
  ); 

  if( showModal ){ return el; }
  if( !showModal ){ return null; }

};

export { UserEditor };
import React from "react";
import { useQuery } from "@apollo/client";

import { SharedTable } from "./../shared/SharedTable";
import { SharedTableRow } from "./../shared/SharedTableRow";
import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerError } from "./../elements/disclaimererror";

import { NoDataDisclosure } from "./../elements/nodatadisclosure";

import { GET_USERS } from "./../../models-graphql-endpoints/query-users";

const rowStyles = ( user, canEdit ) => canEdit(user) ? { cursor: "pointer"  } : {};

const rowclickhandler = (event,canEdit,onEdit, user) => { 
  event.preventDefault(); // console.log( event.target.id , "was just clicked!!!");
  return canEdit(user) && onEdit(user);
};

const dataList = ( obj ) => { 

  let { data } = obj; 
  // console.log( "userviewer - dataList - data" , data ); 
  
  let { canEdit, onEdit } = obj.props; 

  // console.log( "dataList -- canEdit()", canEdit() );
  // console.log( "onEdit()" , onEdit() );  

  let additionalFields = {
    rowStyles: rowStyles
    , canEdit: canEdit
    , onEdit: onEdit
    , rowclickhandler: rowclickhandler
  }; 

  return data.users.map( ( user, idx ) => { 

    return (<SharedTableRow key={ `${idx}-${user.id}` } user={ user } { ...additionalFields } />);

  } );

};

const UserViewer = ( props ) => {  

  // console.log( "UserViewer props", props ); 
  const { loading, error, data } = useQuery( GET_USERS ); 

  
  if( loading ){ return <DisclaimerLoading />; }
  if( error ){ return <DisclaimerError error={error} />; } 
  
  // console.log( "UserViewer - data" , data );
  // console.log( "UserViewer - data.users" , data.users );
  // console.log( "UserViewer - data.users.length" , data.users.length );

  if( !loading && data.users.length > 0 ){
    return <SharedTable>{ dataList( { data , props } ) }</SharedTable>;
  }
  
  if( !loading && data.users.length === 0 ){
    return <NoDataDisclosure />;
  }

};

UserViewer.defaultProps = {
  canEdit: () => false
  , onEdit: () => null
};

export { UserViewer };
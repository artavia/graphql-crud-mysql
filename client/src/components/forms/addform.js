import React, { useState, useRef, useEffect, useCallback } from "react";
import $ from 'jquery';

import { gql } from "@apollo/client";
import { useMutation } from "@apollo/client";

import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerMutationCalled } from "./../elements/disclaimermutationcalled";
import { DisclaimerError } from "./../elements/disclaimererror";

import { CREATE_USER } from "./../../models-graphql-endpoints/mutation-createuser";
import { GET_USERS } from "./../../models-graphql-endpoints/query-users";

const AddForm = () => { 

  let [ fullname, setFullname ] = useState("");
  let [ jobtitle, setJobtitle ] = useState("");
  let [ email, setEmail ] = useState("");
  

  let newModalRef = useRef(null);
  let newEmailRef = useRef();
  let newFullnameRef = useRef(); 
  let newJobtitleRef = useRef(); 

  let extraModalReference = useCallback( () => {
    if( document.querySelector("#exampleModal") !== null ){
      newModalRef.current = document.querySelector("#exampleModal"); // console.log( "newModalRef" , newModalRef );
    }
    return () => {
      newModalRef.current = null;
    };
  } , [] );

  let statefulmarkers = useCallback( () => {

    setFullname("");
    setJobtitle("");
    setEmail("");

    return () => {
      setFullname(null);
      setJobtitle(null);
      setEmail(null);
    };

  } , [] );

  useEffect( () => {
    extraModalReference();
    statefulmarkers();
  } , [ extraModalReference , statefulmarkers ] );

  let completedCB = () => { 
    // console.log("Mutation has completed"); 
    toggleModalClosed();
  };

  const toggleModalClosed = () => {
    $(newModalRef.current).modal( 'hide' );
  };

  const [ 
    createUser
    , { loading, error, called } 
  ] = useMutation(
    CREATE_USER 
    , {
      
      onCompleted: completedCB

      , update( 
        cache, { data: { createUser } } 
      )
      {
        cache.modify( {

          fields: {

            users( existingUsers = [] ){
              
              const newUserRef = cache.writeFragment( {

                data: createUser

                , fragment: gql`
                  fragment NewUser on User{
                    id
                    fullname
                    jobtitle
                    email
                  }
                `
              } );

              return [ ...existingUsers, newUserRef ];

            }

          }

        } );
      }

      , refetchQueries: [ { query: GET_USERS } ] 
      
      , awaitRefetchQueries: true 

    }
  );

  const onChangeFullname = ( event ) => { 
    setFullname( newFullnameRef.current.value ); 
  };

  const onChangeJobtitle = ( event ) => { 
    setJobtitle( newJobtitleRef.current.value ); 
  };

  const onChangeEmail = ( event ) => { 
    setEmail( newEmailRef.current.value ); 
  };

  const handleSubmit = async (event) => {

    event.preventDefault();
    
    // console.log("fullname" , fullname ); 
    // console.log("jobtitle" , jobtitle ); 
    // console.log("email" , email ); 

    createUser( {
      variables: { fullname: fullname , jobtitle: jobtitle , email: email }
    } );

    // console.log( "Form has been submitted" );

  };
  
  return (
    <>
      <div className="container">

        <form className="form" onSubmit={ handleSubmit } >
          
          <div className="form-group">
            <label htmlFor="fullname">Full Name</label>
            <input type="text" ref={newFullnameRef} name="fullname" id="fullname" className="form-control rounded-0" placeholder="Joe Blow" value={ fullname } onChange={ onChangeFullname } />
          </div>

          <div className="form-group">
            <label htmlFor="jobtitle">Job Title</label>
            <input type="text" ref={newJobtitleRef} name="jobtitle" id="jobtitle" className="form-control rounded-0" placeholder="Boatswain" value={ jobtitle } onChange={ onChangeJobtitle } />
          </div>

          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input type="text" ref={newEmailRef} name="email" id="email" className="form-control rounded-0" placeholder="john.doe@mia.net" value={ email } onChange={ onChangeEmail } />
          </div>

          <div className="form-group">
            <button type="submit" disabled={ email === '' || jobtitle === '' || fullname === '' } className="btn btn-primary float-right mr-1">Submit</button>
            <button type="button" className="btn btn-secondary float-right mr-1" onClick={toggleModalClosed}>Cancel</button>
          </div>

        </form>

        { called && (<DisclaimerMutationCalled />) }
        
        { loading && (<DisclaimerLoading />) }

        { error && (<DisclaimerError error={error} />) }
        

      </div>
    </>
  ); 
  
};

export { AddForm };

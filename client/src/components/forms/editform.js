import React, { useState, useRef, useEffect, useCallback } from "react";
import $ from 'jquery';

import { gql } from "@apollo/client";
import { useMutation } from "@apollo/client";

import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerMutationCalled } from "./../elements/disclaimermutationcalled";
import { DisclaimerError } from "./../elements/disclaimererror";

import { UPDATE_USER } from "./../../models-graphql-endpoints/mutation-updateuser";
import { GET_USERS } from "./../../models-graphql-endpoints/query-users";
import { DELETE_USER } from "./../../models-graphql-endpoints/mutation-deleteuser";

const EditForm = ( props ) => { // console.log( "props" , props );

  let { user } = props; // console.log( "user", user );
  
  let { finalizeUserHandler } = props; // console.log( "finalizeUserHandler", finalizeUserHandler ); 

  let [ id, setId ] = useState(null);
  let [ fullname, setFullname ] = useState("");
  let [ jobtitle, setJobtitle ] = useState(""); 
  let [ email, setEmail ] = useState("");
  
  let newModalRef = useRef(null);
  let userIdRef = useRef(); 
  let userFullnameRef = useRef(); 
  let userJobtitleRef = useRef(); 
  let userEmailRef = useRef(); 

  let extraModalReference = useCallback( () => {
    if( document.querySelector("#exampleModal") !== null ){
      newModalRef.current = document.querySelector("#exampleModal"); // console.log( "newModalRef" , newModalRef );
    }
    return () => {
      newModalRef.current = null;
    };
  } , [] );

  let statefulmarkers = useCallback( () => {

    setId(user.id);
    setFullname(user.fullname);
    setJobtitle(user.jobtitle);
    setEmail(user.email);

    return () => {
      setId(null);      
      setFullname(null);
      setJobtitle(null);
      setEmail(null);
    };

  } , [ user ] );

  useEffect( () => {
    extraModalReference();
    statefulmarkers();
  } , [ extraModalReference , statefulmarkers ] );

  
  let completedCB = () => { 
    // console.log("Mutation has completed"); 
    toggleModalClosed();
  };

  const deleteOneRecord = (id) => {
    
    deleteUser( { 
      variables: { id: id } 
    } );
    
  };

  const toggleModalClosed = () => {
    $(newModalRef.current).modal( 'hide' );
    finalizeUserHandler();
  };

  const [ 
    deleteUser 
    , { loading: deleteLoading , error: deleteError, called: deleteCalled } 
  ] = useMutation(
    DELETE_USER
    , {
      onCompleted: completedCB
      
      , refetchQueries: [ { query: GET_USERS } ]
      , awaitRefetchQueries: true 
    }
  );

  const [ 
    updateUser
    , { loading, error, called } 
  ] = useMutation( 
    UPDATE_USER 
    , {
      
      onCompleted: completedCB

      , update( 
        cache, { data: { updateUser } } 
      )
      {
        cache.modify( {
          
          fields: {
            
            users( existingUsers = [] , { readField } ){
              
              const existingUserRef = cache.writeFragment( {

                data: updateUser

                , fragment: gql`
                  fragment ExistingUser on User{
                    id
                    fullname
                    jobtitle
                    email
                  }
                `
              } );

              if( existingUsers.some( ref => readField( "id", ref ) === existingUserRef.id ) ){
                return existingUsers;
              }
              if( !existingUsers.some( ref => readField( "id", ref ) === existingUserRef.id ) ){
                return [ ...existingUsers, existingUserRef ];
              }

            }

          }

        } );
      }

      , refetchQueries: [ { query: GET_USERS } ] 
      
      , awaitRefetchQueries: true 

    }
  );

  const onChangeFullname = ( event ) => { 
    setFullname( userFullnameRef.current.value ); 
  };

  const onChangeJobtitle = ( event ) => { 
    setJobtitle( userJobtitleRef.current.value ); 
  };

  const onChangeEmail = ( event ) => { 
    setEmail( userEmailRef.current.value ); 
  };

  const handleSubmit = async (event) => {

    event.preventDefault();

    let id = (userIdRef.current !== undefined ) ? userIdRef.current.value : null;
    let fullname = (userFullnameRef.current !== undefined ) ? userFullnameRef.current.value : null;
    let jobtitle = (userJobtitleRef.current !== undefined ) ? userJobtitleRef.current.value : null;
    let email = (userEmailRef.current !== undefined ) ? userEmailRef.current.value : null ;

    // console.log("id" , id ); 
    // console.log("fullname" , fullname ); 
    // console.log("jobtitle" , jobtitle ); 
    // console.log("email" , email ); 

    updateUser( { 
      variables: { id: id , fullname: fullname , jobtitle: jobtitle , email: email } 
    } );

    // console.log( "Form has been submitted" );

  }; 

  let customform = (
    <>
      <div className="container">
        
        <form className="form" onSubmit={ handleSubmit } >
          
          <div className="form-group">
            <label htmlFor="jobtitle">
              <h5 className="h4">User <code>{fullname}</code></h5>
              <h6 className="h5">ID <code>{id}</code></h6>
            </label>

            <input type="hidden" ref={userIdRef} name={`id-${id}`} id={`id-${id}`} defaultValue={ id } />

          </div>

          <div className="form-group">
            <label htmlFor="fullname">Full Name</label>
            <input type="text" ref={userFullnameRef} name="fullname" id="fullname" className="form-control rounded-0" placeholder="Joe Blow" value={ fullname } onChange={ onChangeFullname } />
          </div>

          <div className="form-group">
            <label htmlFor="jobtitle">Job Title</label>
            <input type="text" ref={userJobtitleRef} name="jobtitle" id="jobtitle" className="form-control rounded-0" placeholder="Boatswain" value={ jobtitle } onChange={ onChangeJobtitle } />
          </div>

          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input type="text" ref={userEmailRef} name="email" id="email" className="form-control rounded-0" placeholder="john.doe@mia.net" value={ email } onChange={ onChangeEmail } />
          </div>

          <div className="form-group">
            <button type="submit" disabled={ fullname === '' || jobtitle === '' || email === '' } className="btn btn-primary float-right mr-1">Submit</button>
            <button type="button" className="btn btn-secondary float-right mr-1" onClick={toggleModalClosed}>Cancel</button>
            <button type="button" className="btn btn-danger float-right mr-1" onClick={ () => deleteOneRecord(id)}>Delete User</button>
          </div>

        </form>

        { called && (<DisclaimerMutationCalled />) }
        { loading && (<DisclaimerLoading />) }
        { error && (<DisclaimerError error={error} />) }

        { deleteCalled && (<DisclaimerMutationCalled />) }
        { deleteLoading && (<DisclaimerLoading />) }
        { deleteError && (<DisclaimerError error={deleteError} />) }
        
      </div>
    </>
  );

  return customform;
  
};

export { EditForm };

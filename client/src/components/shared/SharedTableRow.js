import React from "react";

const SharedTableRow = ( props ) => { 

  // console.log( "SharedTableRow props", props );
  
  const { canEdit, onEdit, rowStyles, rowclickhandler } = props;
  
  // console.log( "SharedTableRow -- canEdit()", canEdit() );

  const { user } = props;
  const { id, fullname, jobtitle, email } = user; 
  
  let additionalProps; 
  
  if( !canEdit() ){ 
    additionalProps = {}; 
  }
  
  if( canEdit() ){
    additionalProps = {
      user: user,
      style: rowStyles( user, canEdit )
      , onClick: event => rowclickhandler( event, canEdit, onEdit, user )
    };
  }

  let el = (
    <>
      <tr key={ user.id } {...additionalProps} >
        <th id={`id-${id}`} scope="row">{id}</th>
        <td id={`fullname-${id}`}>{fullname}</td>
        <td id={`jobtitle-${id}`}>{jobtitle}</td>
        <td id={`email-${id}`}>{email}</td>
      </tr>
    </>
  );
  
  return el;

};

export { SharedTableRow };
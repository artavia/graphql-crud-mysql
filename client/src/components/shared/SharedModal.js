import React from "react";
import { useEffect } from "react"; 
import { useCallback } from "react"; 
import { useRef } from "react"; 
import $ from 'jquery';

const SharedModal = ( props ) => { 
  
  let { finalizeUserHandler } = props; 
  let { user } = props;
  let modalRef = useRef();

  // console.log( "SharedModal props" , props ); 
  // console.log( "SharedModal finalizeUserHandler" , finalizeUserHandler );

  const blingAndBoots = useCallback( () => { 
    // console.log( "$", $ );
    $(modalRef).modal('show');
    $(modalRef).on( 'hidden.bs.modal', finalizeUserHandler );
  } , [ finalizeUserHandler ] );

  useEffect( () => {
    blingAndBoots();
  } , [ blingAndBoots ] );

  // const toggleModalClosed = () => {
  //   $(modalRef).modal( 'hide' );
  //   finalizeUserHandler();
  // };

  let el = (
    <div 
      className="modal fade" 
      ref={ modal => modalRef = modal }
      id="exampleModal" 
      tabIndex="-1" 
      role="dialog" 
      aria-labelledby="exampleModalLabel" 
      aria-hidden="true"
    >
      
      <div className="modal-dialog" role="document">
        <div className="modal-content">

          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">{!user.id ?"Add New User":"Edit User"}</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div className="modal-body">
            { props.children }
          </div>

          {/* <div className="modal-footer">
            <button type="button" className="btn btn-secondary" onClick={toggleModalClosed}>Cancel</button>
          </div> */}

        </div>
      </div>

    </div>
  );

  return el;
};

export { SharedModal };
import React from "react";

const NoDataDisclosure = () => {

  let el = (
    <>
      <div className="container">
        <div className="row">
          
          <div className="col-md-6 mx-auto">
          
            <div className="card mt-5 col-sm">
              <div className="card-body">
                There are not any posts.
              </div>
            </div>
            
          </div>

        </div>
      </div>
    </>
  );

  return el;
};

export { NoDataDisclosure };
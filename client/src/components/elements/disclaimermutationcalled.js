import React from "react";

const DisclaimerMutationCalled = () => {

  let el = (
    <>
      <div className="container">
        <div className="row">
          <p>Form submission is processing.</p>
        </div>
      </div>
    </>
  );

  return el;
};

export { DisclaimerMutationCalled };
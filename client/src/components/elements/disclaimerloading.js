import React from "react";

const DisclaimerLoading = () => {

  let el = (
    <>
      <div className="container">
        <div className="d-flex justify-content-center">
          <div className="spinner-border text-warning" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      </div>
    </>
  );

  return el;
};

export { DisclaimerLoading };
import { ApolloClient, InMemoryCache, HttpLink, ApolloLink, from } from "@apollo/client";

const cache = new InMemoryCache( {
  
  typePolicies: {
    
    Query: {

      fields: {
        
        users: {  // either of the two will work successfully...
          
          merge: false  // Short for always preferring incoming over existing data

          // merge( existing , incoming ){
          //   return incoming;  // Equivalent to what happens if there is no custom merge function
          // }
        }

      }

    }

    , User: { 
      // keyFields: ["id"]  , 
      fields: ["id", "fullname", "jobtitle", "email"]
    } 

  }
  
} );

const httpLink = new HttpLink( {
  uri: `http://localhost:4000/graphql` 
} );

const testLink = new ApolloLink( (operation, forward) => { 
  // add the "custom-name" and "custom-version" custom headers to the default headers
  operation.setContext( ( { headers = {} } ) => {
    return ({ 
      headers: {
        ...headers
        , "custom-name": "WidgetX Ecom [web]"
        // , "custom-version": "1.51.50"
        , "custom-version": "5.1.50.ou812"
      }
    });
  } );
  return forward(operation);
} );

const contentTypeLink = new ApolloLink( (operation, forward) => { 
  // add the "custom-name" and "custom-version" custom headers to the default headers
  operation.setContext( ( { headers = {} } ) => {
    return ({ 
      headers: {
        ...headers

        , "Content-Type": "application/graphql; charset=utf-8"
        , "Accept": "application/graphql" 
        
      }
    });
  } );
  return forward(operation);
} );

const client = new ApolloClient( {

  // REQUIRED CONSTRUCTOR FIELDS
  cache: cache

  // , link: httpLink // v0 - original  
  , link: from( [ testLink, contentTypeLink, httpLink ] )  
  

  // OPTIONAL CONSTRUCTOR FIELDS

  , defaultOptions: {

    watchQuery: { 
      notifyOnNetworkStatusChange: true 
      , errorPolicy: "all"       
      , fetchPolicy: "cache-and-network"
    }

    , query: {
      errorPolicy: "all"
      , fetchPolicy: "network-only"
    }
    
    , mutate: {
      
      errorPolicy: "none" 
      , fetchPolicy: "no-cache" 

    }
  }

} );

export { client };
import { gql } from "@apollo/client";

const GET_USER = gql`
  query GetUser( $id: ID! ) {
    user( id:$id ) {
      id
      fullname
      jobtitle
      email
    }
  }
`;

export { GET_USER };

import { gql } from "@apollo/client";

const UPDATE_USER = gql`
  mutation UpdateUser( $id: ID!, $fullname: String!, $email: String!, $jobtitle: String! ) {
    updateUser( id: $id , fullname: $fullname , email: $email, jobtitle: $jobtitle )
  }
`;

export { UPDATE_USER };

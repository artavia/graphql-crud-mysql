import { gql } from "@apollo/client";

const CREATE_USER = gql`
  mutation CreateUser( $fullname: String! , $email: String! , $jobtitle: String! ) {
    createUser( fullname: $fullname , email: $email , jobtitle: $jobtitle )
  }
`;

export { CREATE_USER };

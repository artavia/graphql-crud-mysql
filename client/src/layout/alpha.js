import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Redirect } from "react-router-dom";

import { Home } from "./../components/pages/home";
import { Admin } from "./../components/pages/admin";
import { AlphaTest } from "./../components/pages/alphatest";
import { DNEPage } from "./../components/pages/DNEPage";
import { Main } from "./main";

// NEW EXPORT
const Alpha = () => {

  // In order for the Nav to behave properly, the order of the stack is changed 
  // from ... BrowserRouter > Main > Security > Switch > ETC...
  // to   ... BrowserRouter > Security > Main > Switch > ETC...
  
  const el = (
    <BrowserRouter>
      <Main>
        <Switch>

          {/* FROM THE TOP DOWN THE ORDER MUST STAY THE SAME... */}
          
          <Route exact={ true } path="/" component={ Home } />

          <Route exact={true} path="/admin" component={ Admin }/>
          
          <Route exact={true} path="/alphatest" component={ AlphaTest }/>

          {/* Handling 404s ( either/or ) - change the url */}            
          <Route path="/404" component={ DNEPage } />
          <Redirect from="*" to="/404" />
          
          {/* Handling 404s ( either/or ) - do not change the url */}
          {/* <Route component={ DNEPage } /> */}

        </Switch>
      </Main>
    </BrowserRouter>
  );

  return el;
}; 

export { Alpha };
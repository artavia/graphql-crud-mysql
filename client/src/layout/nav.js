import React from "react";
import { NavLink } from "react-router-dom";

const Nav = () => {

  const noOpLink = ( event ) => { event.preventDefault(); };

  const el = (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      
      <NavLink className="navbar-brand" to={"/"} onClick={ noOpLink }>GraphQL&#45;MySQL App</NavLink>
      
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">

        <ul className="navbar-nav mr-auto">

          <li className="nav-item">
            <NavLink className="nav-link" to={"/"} activeClassName={"active"} exact>
              Home
            </NavLink>
          </li>
          
          <li className="nav-item">
            <NavLink className="nav-link" to={"/admin"} activeClassName={"active"} exact>
              Admin
            </NavLink>
          </li> 

          <li className="nav-item">
            <NavLink className="nav-link" to={"/alphatest"} activeClassName={"active"} exact>
              Alpha Test
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className="nav-link" to={"/moarrtest"} activeClassName={"active"} exact>
              MOARRR TEST!!!
            </NavLink>
          </li>
          
        </ul>

        <ul className="navbar-nav">

          <li className="nav-item">
            
            <NavLink role="button" className="btn btn-outline-success navbar-btn my-2 my-sm-0 mr-sm-2" to={"/moarrtest"} activeClassName={"active"} exact>
              MOARRR TEST!!!
            </NavLink>

          </li>

        </ul>

      </div>

    </nav>
  );

  return el;
}

export { Nav };

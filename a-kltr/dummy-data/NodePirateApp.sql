-- -------------------
SET FOREIGN_KEY_CHECKS = 0;

-- -----------------
CREATE DATABASE IF NOT EXISTS `NodePirateApp`
	CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `NodePirateApp`;

-- -----------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
	id INTEGER NOT NULL AUTO_INCREMENT,
	fullname VARCHAR(50) NOT NULL,
	jobtitle VARCHAR(50) NOT NULL,
	email VARCHAR(150) NOT NULL,
	CONSTRAINT pirate_pk PRIMARY KEY(id),
	UNIQUE KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='' AUTO_INCREMENT=1;


-- -----------------
SET FOREIGN_KEY_CHECKS = 1;

-- -------------------
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;

-- -------------------
INSERT INTO NodePirateApp.users (`fullname`,`jobtitle`,`email`) VALUES ( "Monkey D. Luffy","Captain","luffy@mugiwara.com" ), ( "Roronoa Zoro","Swordsman","zoro@mugiwara.com" ), ( "Nami Melanine","Navigator","nami@mugiwara.com" ), ( "Usopp Sogekingu","Sniper","usopp@mugiwara.com" ), ( "Sanji Vinsmoke","Cook","sanji@mugiwara.com" ), ( "Tony Tony Chopper","Dokuta","chopper@mugiwara.com" ), ( "Nico Robin","Archaelogist","robin@mugiwara.com" ), ( "Cyborg Franky","Boatswain","franky@mugiwara.com" ), ( "Brooke Burrukku","Musician","brooke@mugiwara.com" ), ( "Cerberus","Monster","cerberus@thrillerbark.com" ) ; 

-- -------------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

-- -------------------
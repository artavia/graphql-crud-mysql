# GraphQL, CRUD, MySQL

## Description
A modernized, extended and repurposed version of a project by Ms. Avanthika Meenakshi on the subjects of @apollo/client, React, and MySQL for NodeJS found at the LogRocket blog. 

### Back End Processing and/or completion date(s)
  - September 17, 2020; September 18, 2020; September 19, 2020; September 20, 2020; September 21, 2020.

### Front Facing Processing and/or completion date(s)
  - September 21, 2020; September 22, 2020; September 23, 2020.

## Attribution(s)

There is no original write-up in terms of this project. I have actually drawn from some limited previous experience I had with mysql and node. 

That original lesson(s) have some age to them. I processed these, then, forgot about them. Those are [Node.js for Beginners](https://code.tutsplus.com/tutorials/nodejs-for-beginners--net-26314 "link to tutsplus"), as well as, [Using MySQL with Node.js and the node-mysql JavaScript Client](https://www.sitepoint.com/using-node-mysql-javascript-client/ "link to sitepoint"). 

Then, in 2020 I queried **&quot;graphql sqlite crud example&quot;** at **duck duck go**. That is when I came up with the following article by Avanthika Meenakshi called [Make CRUD simple with Node, GraphQL, and React](https://blog.logrocket.com/crud-with-node-graphql-react/ "link to logrocket blog") which can be found at the logrocket blog. The search results were slightly off the mark but the subject of GraphQL is totally relevant to front-end development skills.

And, for the sake of posterity and so as to not reinvent the wheel, I am using my own database in lieu of the one provided by Ms. Meenakshi. That, too, is included. 

## Four different back&#45;end apps are included
In addition to utilizing mysql package, I am also including the mysql2 package so that nothing is left out. Those can be found here:
  - [mysql](https://www.npmjs.com/package/mysql "link to mysql")
  - [mysql2](https://www.npmjs.com/package/mysql2 "link to mysql2")

The **mysql** package is more renowned and intuitive in nature. But the **mysql2** package is very much promise&#45;based and it has the advantage of utilizing **Portable Delivery Objects** (aka PDOs) to reduce the likelihood of sql injections by means of utilizing **prepared statements**.

Therefore, I have not included just **one** server. Rather, I have included (uno, dos, tres&hellip;) **four** different servers for back-end use. Two utilize **mysql** while the other two utilize **mysql2**. Plus, each group splinters off further in utilizing each **apollo-server-express**, as well as, **express-graphql**. These inclusions should provide a little something for everybody. 

On the other hand, if you want to see what it means to use **optimistic response** in terms of mutation composition on your front-end (because you will not find it here), then, you have to visit my previous project at gitlab entitled [graphql-react-express-okta](https://gitlab.com/artavia/graphql-react-express-okta "link to GraphQL React Express Okta repo"). You may have to perform some additional due&#45;diligence but that is a small price to pay for the take&#45;away and payoff with which you get to walk away as yours!

### May Jesus bless you
I hope to have influenced you in some positive manner today. 
// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// =============================================
const { NODE_ENV, PORT, HOSTNAME } = process.env;

// =============================================
// Establish the NODE_ENV toggle settings
// ==============================================
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development';

// =============================================
// BASE SETUP
// =============================================
const express = require("express");
const app = express();

// =============================================
// new boilerplate
// =============================================
const cors = require("cors");
const path = require("path");

// =============================================
// IMPORT mysql2 MDUB
// =============================================
const { createPool } = require("./custom-middleware/mysql2-createPool");

// =============================================
// graphql boilerplate
// graphql middleware - schemas and resolvers AND context
// =============================================
const { ApolloServer } = require("apollo-server-express"); 

const { typeDefs } = require("./graphql-mysql2/apollo-server-express/typedefs");
const { resolvers } = require("./graphql-mysql2/apollo-server-express/resolvers");
const { context } = require("./graphql-mysql2/apollo-server-express/context");


// =============================================
// USE PARAMS - FINER DETAILS
// =============================================
app.use( cors() );
app.use( express.json() ); 
app.use( express.urlencoded( { extended: true } ) ); 

// =============================================
// IMPLEMENT MYSQL DB MIDDLEWARE
// =============================================

app.use( createPool );

// =============================================
// IMPLEMENT GRAPHQL ROUTE
// =============================================
const server = new ApolloServer( {
  typeDefs: typeDefs
  , resolvers: resolvers
  , context: context
  , playground: IS_PRODUCTION_NODE_ENV ? false : true  // , playground: process.env.NODE_ENV === 'development'
} );

server.applyMiddleware( { app: app } );


// =============================================
// Serve static files from the React app
// The "catchall" handler that will handle all requests in order to return React's index.html file.
// Be sure that NODE_ENV is configured to "production" in the server dot env file
// or else "Error: ENOENT:" will show up in the server-side terminal
// =============================================
if( (IS_PRODUCTION_NODE_ENV === true) ){
  
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 

  const catchAllGET = ( req, res, next ) => {
    res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
  };
  app.get( '*' , catchAllGET ); 

}

// =============================================
// DB and BACKEND SERVERS INSTANTIATION
// =============================================
app.listen( PORT, HOSTNAME, () => { 
  if( IS_PRODUCTION_NODE_ENV === true ){
    // console.log( `\nProduction backend is running, baby. \n`);
    console.log( `\nProduction backend is running at http://${HOSTNAME}:${PORT} . \n`);
  }
  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `\nDevelopment backend has started at http://${HOSTNAME}:${PORT} . \n`);
    console.log(`\nOpen the GraphQL playground at http://${HOSTNAME}:${PORT}${server.graphqlPath} \n`);
  }
} );

// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/apollo-server-express
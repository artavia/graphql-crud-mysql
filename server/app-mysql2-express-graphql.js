// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// =============================================
const { NODE_ENV, PORT, HOSTNAME } = process.env;

// =============================================
// Establish the NODE_ENV toggle settings
// ==============================================
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development';

// =============================================
// BASE SETUP
// =============================================
const express = require("express");
const app = express();

// =============================================
// new boilerplate
// =============================================
const cors = require("cors");
const path = require("path");

// =============================================
// IMPORT mysql2 MDUB
// =============================================
const { createPool } = require("./custom-middleware/mysql2-createPool");

// =============================================
// graphql boilerplate
// graphql middleware - schemas and resolvers AND context
// =============================================
const { graphqlHTTP } = require("express-graphql");

const { schema } = require("./graphql-mysql2/express-graphql/schema");
const { rootValue } = require("./graphql-mysql2/express-graphql/resolvers");
const { context } = require("./graphql-mysql2/express-graphql/context");


// =============================================
// USE PARAMS - FINER DETAILS
// =============================================
app.use( cors() );
app.use( express.json() ); 
app.use( express.urlencoded( { extended: true } ) ); 

// =============================================
// IMPLEMENT MYSQL DB MIDDLEWARE
// =============================================

app.use( createPool );

// =============================================
// IMPLEMENT GRAPHQL ROUTE
// =============================================
app.use( '/graphql' , graphqlHTTP( (req, res) => { 
  return ({
    schema: schema
    , rootValue: rootValue
    , context: context( { req, res } )
    , graphiql: IS_PRODUCTION_NODE_ENV ? false : { headerEditorEnabled: true }  
  });
} ) ); 


// =============================================
// Serve static files from the React app
// The "catchall" handler that will handle all requests in order to return React's index.html file.
// Be sure that NODE_ENV is configured to "production" in the server dot env file
// or else "Error: ENOENT:" will show up in the server-side terminal
// =============================================
if( (IS_PRODUCTION_NODE_ENV === true) ){
  
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 

  const catchAllGET = ( req, res, next ) => {
    res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
  };
  app.get( '*' , catchAllGET ); 

}

// =============================================
// DB and BACKEND SERVERS INSTANTIATION
// =============================================
app.listen( PORT, HOSTNAME, () => {   
  if( IS_PRODUCTION_NODE_ENV === true ){
    // console.log( `\nProduction backend is running, baby. \n`);
    console.log( `\nProduction backend is running at http://${HOSTNAME}:${PORT} . \n`);
  }
  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `\nDevelopment backend has started at http://${HOSTNAME}:${PORT} . \n`);
    console.log(`\nOpen the GraphQL playground at http://${HOSTNAME}:${PORT}/graphql \n`);
  }
} );

// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/express-graphql
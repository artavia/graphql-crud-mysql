const { buildASTSchema } = require("graphql");
const gql = require("graphql-tag");

// =============================================
// Construct a schema, using GraphQL schema language
// schema / typeDef declarations
// =============================================

const schema = buildASTSchema(gql`
  
  # INPUTS 
  # ...

  # OBJECT TYPES 
  
  type User {
    id: ID
    fullname: String!
    jobtitle: String!
    email: String!
  }

  # SCALAR-BASED QUERY TYPES
  
  type Query {

    # ...initial test(s)
    hello: String
    dbtest: String

    # ...the rest
    users: [ User ]
    user( id: ID! ) : User
  }
  
  # MUTATION TYPES 

  type Mutation {
    updateUser( id: ID!, fullname: String!, email: String!, jobtitle: String! ): Boolean
    createUser( fullname: String!, email: String!, jobtitle: String! ): Boolean
    deleteUser( id: ID! ): Boolean     
  }

`);

module.exports = { schema };
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/express-graphql
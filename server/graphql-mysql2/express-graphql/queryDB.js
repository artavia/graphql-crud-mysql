const queryDB = async ( req, sql, args ) => {

  try{
    const result = await req.mysqlDb.execute( sql, args ); // await console.log( "queryDB PDO result[0]" , await result[0] ); 
    if( await result !== undefined ){
      return await result[0].changedRows > 0 || await result[0].affectedRows > 0 || await result[0].insertId ? true : await result;
    }
  }
  catch(error){
    
    // await console.log( "queryDB PDO ERROR" , await error );
    // throw await error;

    let blotto = {
      err: await error
      , connection: await req.mysqlDb
    };
    throw await blotto;
  }
};

module.exports = {queryDB};
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/express-graphql
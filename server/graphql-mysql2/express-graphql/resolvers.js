// =============================================
// Helper function(s)
// =============================================

const { queryDB } = require("./queryDB");

const catchStatement = async ( obj ) => {
  let { err, connection } = await obj;  // req.mysqlDb ~~ connection
  
  // await console.error( "err" , await err + '\r\n');
  // await console.error( 'stack: ' + await err.stack + '\r\n');
  await console.error( 'code:' + ' ' + await err.code + '\r\n');
  await console.error( 'fatal:' + ' ' + await err.fatal + '\r\n');

  // await console.log( "\nconnection\n" , await connection );

  // ---------------------------------
  // await connection.destroy(); // destroy is not a function
  // await connection.pool.destroy(); // destroy is not a function
  // console.error( 'The connection has been destroyed-- count it!!!\r\nThe connection\'s end method will throw it\'s own error in ANOTHER 30 seconds from now.');

  // ---------------------------------
  await connection.pool.end( async ( err ) => { if( await err){ await console.log("End method -- an error has occurred..." , await err );} else { await console.log("The connection is terminated now." ); } } ); 
  await connection.pool.releaseConnection( await connection ); // await console.log( "connection.pool" , await connection.pool );
  await console.log("That POOL INSTANCE is RELEASED and GRACEFULLY ENDED... ");

};

// =============================================
// GraphQL Endpoints
// ( args / __ , context, info )   // NO parent / _ 
// =============================================

const rootValue = {

  // Queries 
  // ...initial test(s)

  hello: async ( args, context, info ) => { 

    // await console.log( "args" , await args );       // RETURNS {}
    // await console.log( "context" , await context ); // RETURNS {}
    // await console.log( "info" , await info );       // RETURNS { TOO BEAUCOUP FOR YOU... } 

    // const { myProperty, myReqHeaders } = await context;
    // const { customVersion } = await context;
    // await console.log( "myProperty" , await myProperty ); // 'Scooby, Scooby Doo!!!'
    // await console.log( "myReqHeaders" , JSON.stringify( await myReqHeaders, null, 2 ) ); 
    // await console.log( "customVersion" , await customVersion ); // { "custom-version": "5.1.50.ou812" }

    return await "Konichiwa, whirlled world!";
  } ,

  // dbtest: async ( args, req ) => {
  dbtest: async ( args, context ) => {
    
    const { req } = await context; 
    return await queryDB( await req, 'SELECT 1 + 1 AS solution;' )
    .then( ( data ) => { 
      
      const rows = data[0]; // console.log( "rows" , rows );
      // const columns = data[1]; // console.log( "columns" , columns );
      
      return "The solution is: " + rows[0].solution;
    } )
    .catch( catchStatement ); 
  }, 

  // Queries 
  // ...the rest

  users: async ( args, context ) => {
    
    const { req } = await context; // await console.log( "req", await req );

    // https://dev.mysql.com/doc/mysql-errors/8.0/en/server-error-reference.html
    // SELECT * FROM no_such_table;

    // return await queryDB( await req, "SELECT * FROM no_such_table;" )
    return await queryDB( await req, "SELECT * FROM users;" )
    .then( ( data ) => { 
      // console.log( "data" , data );
      // console.log( "data[0]" , data[0] ); 
      return data[0];
    } )
    .catch( catchStatement );
  }
  
  , user: async ( args, context ) => {
    
    const { req } = await context;
    return await queryDB( await req, "SELECT * FROM users WHERE id = ?;", [ args.id ] )
    .then( ( data ) => {
      // console.log( "data[0]" , data[0] ); 
      return data[0][0];
    } )
    .catch( catchStatement );
  }
  
  // Mutations

  , updateUser: async ( args, context ) => {

    const { req } = await context;
    return await queryDB( await req, "UPDATE users SET fullname = ?, email = ?, jobtitle = ? WHERE id = ?;", [ args.fullname, args.email, args.jobtitle, args.id ] )
    .then( ( data ) => {
      // console.log( "updateUser data" , data );
      return data;
    } )
    .catch( catchStatement );
  }

  , createUser: async ( args, context ) => {

    const { req } = await context;
    return await queryDB( await req, "INSERT INTO users SET fullname = ?, email = ?, jobtitle = ?;", [ args.fullname, args.email, args.jobtitle ] )
    .then( ( data ) => { 
      // console.log( "createUser data" , data );
      return data; 
    } )
    .catch( catchStatement );
  }

  , deleteUser: async ( args, context ) => {

    const { req } = await context;
    return await queryDB( await req, "DELETE FROM users WHERE id = ?;", [ args.id ] )
    .then( ( data ) => { 
      // console.log( "deleteUser data" , data );
      return data;
    } )
    .catch( catchStatement );
  }
  
};

module.exports = { rootValue };
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/express-graphql
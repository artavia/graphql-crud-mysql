// =============================================
// Helper function(s)
// =============================================

const { queryDB } = require("./queryDB");

const catchStatement = async ( obj ) => {
  let { err, connection } = await obj;  // req.mysqlDb ~~ connection

  // await console.error( "err" , await err + '\r\n');
  // await console.error( 'stack: ' + await err.stack + '\r\n');
  await console.error( 'code:' + ' ' + await err.code + '\r\n');
  await console.error( 'fatal:' + ' ' + await err.fatal + '\r\n');

  // ---------------------------------
  await connection.pool.end( async ( err ) => { if( await err){ await console.log("End method -- an error has occurred..." , await err );} else { await console.log("The connection is terminated now." ); } } ); 
  await connection.pool.releaseConnection( await connection ); 
  await console.log("That POOL INSTANCE is RELEASED and GRACEFULLY ENDED... ");  

};

// =============================================
// GraphQL Endpoints
// ( parent / _, args / __ , context, info ) 
// =============================================

const resolvers = {
  
  Query: {

    // Queries 
    // ...initial test(s)

    hello: async ( _, __, context, info ) => {

      // await console.log( "_" , _ );        // RETURNS {}
      // await console.log( "__" , __ );       // RETURNS {}
      // await console.log( "context" , await context ); // RETURNS {}
      // await console.log( "info" , await info );    // RETURNS { TOO BEAUCOUP FOR YOU... }

      // await console.log( "context.myProperty" , await context.myProperty ); // 'Scooby, Scooby Doo!!!'
      // await console.log( "context.myReqHeaders" , JSON.stringify( await context.myReqHeaders, null, 2 ) );
      // await console.log( "context.customVersion" , await context.customVersion ); // // { "custom-version": "5.1.50.ou812" } 
     
      return await "Konichiwa, whirlled world!";
    } , 

    // dbtest: async ( args, req ) => {
    dbtest: async ( _, __, context ) => {
      
      return await queryDB( await context.req, 'SELECT 1 + 1 AS solution;' )
      .then( ( data ) => { 
        
        const rows = data[0]; // console.log( "rows" , rows );
        // const columns = data[1]; // console.log( "columns" , columns ); 
        
        return "The solution is: " + rows[0].solution;
      } )
      .catch( catchStatement );
    } , 

    // Queries 
    // ...the rest

    users: async ( _, __, context ) => {

      // https://dev.mysql.com/doc/mysql-errors/8.0/en/server-error-reference.html
      // SELECT * FROM no_such_table;
    
      // return await queryDB( await context.req, "SELECT * FROM no_such_table;" )
      return await queryDB( await context.req, "SELECT * FROM users;" )
      .then( ( data ) => {
        // console.log( "data[0]" , data[0] ); 
        return data[0];
      } )
      .catch( catchStatement );
    }

    , user: async ( _, __, context ) => {
    
      return await queryDB( await context.req, "SELECT * FROM users WHERE id = ?;", [ __.id ] )
      .then( ( data ) => {
        // console.log( "data[0]" , data[0] ); 
        return data[0][0];
      } )
      .catch( catchStatement );  
    }

  }
  
  , Mutation: {

    // Mutations

    updateUser: async ( _, __, context ) => {

      return await queryDB( await context.req, "UPDATE users SET fullname = ?, email = ?, jobtitle = ? WHERE id = ?;", [ __.fullname , __.email , __.jobtitle , __.id ] )
      .then( ( data ) => {
        // console.log( "updateUser data" , data );
        return data;
      } )
      .catch( catchStatement );
    }

    , createUser: async ( _, __, context ) => {
      
      return await queryDB( await context.req, "INSERT INTO users SET fullname = ?, email = ?, jobtitle = ?;", [ __.fullname , __.email , __.jobtitle ] )
      .then( ( data ) => {
        // console.log( "createUser data" , data );
        return data;
      } )
      .catch( catchStatement );  
    }

    , deleteUser: async ( _, __, context ) => {
  
      return await queryDB( await context.req, "DELETE FROM users WHERE id = ?;", [ __.id ] )
      .then( ( data ) => {
        // console.log( "deleteUser data" , data );
        return data;
      } )
      .catch( catchStatement );
    }

  }

};

module.exports = { resolvers }; 
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/apollo-server-express
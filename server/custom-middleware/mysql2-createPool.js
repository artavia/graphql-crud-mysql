// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}
const { DBHOST,DBUSER,DBPASSWORD,DBNAME } = process.env;

// =============================================
// mysql boilerplate
// Create MySQL Pool ...Externalized for modularity and organization purposes
// =============================================
const mysql = require("mysql2/promise");

const createPool = ( req, res, next ) => {
  
  req.mysqlDb = mysql.createPool( { 
    
    host: DBHOST
    , database: DBNAME
    , user: DBUSER
    , password: decodeURIComponent(DBPASSWORD)
    // , password: "biyahhh" 
    
    , waitForConnections: true
    , connectionLimit: 10
    , queueLimit: 0
  } ); 

  next();
};

module.exports = { createPool };
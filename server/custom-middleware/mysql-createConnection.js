// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}
const { DBHOST,DBUSER,DBPASSWORD,DBNAME } = process.env;

// =============================================
// mysql boilerplate
// Create MySQL Connection ...Externalized for modularity and organization purposes
// =============================================
const mysql = require("mysql");

const createConnection = ( req, res, next ) => {
  
  req.mysqlDb = mysql.createConnection( {
    host: DBHOST 
    , database: DBNAME
    , user: DBUSER
    , password: decodeURIComponent(DBPASSWORD) 
    // , password: "biyahhh" 
  } );

  req.mysqlDb.connect();
  next();
};

module.exports = { createConnection };
const context = async ( props ) => {

  const { res } = await props;
  const { req } = await props;

  await res.setHeader('Content-Type', 'application/graphql; charset=utf-8'); 

  // await console.log( "context req", await req );

  const customVersion = await req.get('custom-version');
  const myReqHeaders = await req.headers;

  return {
    req: await req
    , myProperty: await "Scooby, Scooby Doo!!!"
    , customVersion: await customVersion
    , myReqHeaders: await myReqHeaders
  };
  
};

module.exports = { context };
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/apollo-server-express
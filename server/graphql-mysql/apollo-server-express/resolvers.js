// =============================================
// Import helper function(s)
// =============================================

const catchStatement = (obj) => { // console.log( "obj", obj );
  let { err, connection } = obj;
  console.error( 'code:' + ' ' + err.code + '\r\n');
  console.error( 'fatal:' + ' ' + err.fatal + '\r\n');
  // console.error( 'stack: ' + err.stack + '\r\n');
  // console.error( "err" , err + '\r\n');

  connection.destroy();
  console.error( 'The connection has been destroyed-- count it!!!\r\nThe connection\'s end method will throw it\'s own error in ANOTHER 30 seconds from now.');
};

const { queryDB } = require("./queryDB");

// =============================================
// GraphQL Endpoints
// ( parent / _, args / __ , context, info ) 
// =============================================

const resolvers = {
  
  Query: {

    // Queries 
    // ...initial test(s)

    // hello: async ( _, __, context, info ) => {
    hello: async ( _, __, context ) => {

      // await console.log( "_" , _ );        // RETURNS {}
      // await console.log( "__" , __ );       // RETURNS {}
      // await console.log( "context" , await context ); // RETURNS {}
      // await console.log( "info" , await info );    // RETURNS { TOO BEAUCOUP FOR YOU... }

      // await console.log( "context.myProperty" , await context.myProperty ); // 'Scooby, Scooby Doo!!!'
      // await console.log( "context.myReqHeaders" , JSON.stringify( await context.myReqHeaders, null, 2 ) );
      await console.log( "context.customVersion" , await context.customVersion ); // // { "custom-version": "5.1.50.ou812" } 
     
      return await "Konichiwa, whirlled world!";
    } , 

    dbtest: async ( _, __, context ) => { 
      return await queryDB( await context.req, 'SELECT 1 + 1 AS solution;' )
      .then( ( rows ) => { // console.log( "rows" , rows ); 
        return "The solution is: " + rows[0].solution; 
      } )
      .catch( catchStatement );
    } , 

    // Queries 
    // ...the rest

    users: async ( _, __, context ) => {
      
      // https://dev.mysql.com/doc/mysql-errors/8.0/en/server-error-reference.html
      // SELECT * FROM no_such_table;
    
      // return await queryDB( await context.req, "SELECT * FROM no_such_table;" )
      return await queryDB( await context.req, "SELECT * FROM users;" )
      .then( ( data ) => { // console.log( "data" , data );
        return data;
      } )
      .catch( catchStatement );
    }

    , user: async ( _, __, context ) => { 
      return await queryDB( await context.req, "SELECT * FROM users WHERE id = ?;", [ __.id ] )
      .then( ( data ) => { // console.log( "data" , data );
        return data[0];
      } )
      .catch( catchStatement );  
    }

  }
  
  , Mutation: {

    // Mutations

    updateUser: async ( _, __, context ) => {
      return await queryDB( await context.req, "UPDATE users SET fullname = ?, email = ?, jobtitle = ? WHERE id = ?;", [ __.fullname , __.email , __.jobtitle , __.id ] )
      .then( ( data ) => { // console.log( "data" , data );
        return data;
      } )
      .catch( catchStatement ); 
    }

    , createUser: async ( _, __, context ) => {
      return await queryDB( await context.req, "INSERT INTO users SET fullname = ?, email = ?, jobtitle = ?;", [ __.fullname , __.email , __.jobtitle ] )
      .then( ( data ) => { // console.log( "data" , data );
        return data;
      } )
      .catch( catchStatement ); 
    }

    , deleteUser: async ( _, __, context ) => {
      return await queryDB( await context.req, "DELETE FROM users WHERE id = ?;", [ __.id ] )
      .then( ( data ) => { // console.log( "data" , data );
        return data;
      } )
      .catch( catchStatement ); 
    }

  }

};

module.exports = { resolvers }; 
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/apollo-server-express
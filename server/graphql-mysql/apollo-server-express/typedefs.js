const { gql } = require("apollo-server-express");

// =============================================
// Construct a schema, using GraphQL schema language
// schema / typeDef declarations
// =============================================

const typeDefs = gql`
  
  # INPUTS 
  # ...

  # OBJECT TYPES 
  type User {
    id: ID
    fullname: String!
    jobtitle: String!
    email: String!
  }

  # SCALAR-BASED QUERY TYPES 
  type Query {
    
    # ...initial test(s)
    hello: String
    dbtest: String
    
    # ...the rest
    users: [ User ]
    user( id: ID! ) : User
  }
  
  # MUTATION TYPES 
  type Mutation { 
    updateUser( id: ID!, fullname: String!, email: String!, jobtitle: String! ): Boolean
    createUser( fullname: String!, email: String!, jobtitle: String! ): Boolean
    deleteUser( id: ID! ): Boolean 
  }

`;

module.exports = { typeDefs };
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/apollo-server-express
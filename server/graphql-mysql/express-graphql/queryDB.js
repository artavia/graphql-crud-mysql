const queryDB = (req, sql, args) => {
  
  return new Promise((resolve, reject) => {
  
    // console.log( "req" , req );  
  
    req.mysqlDb.query( sql, args, (err, rows) => {

      if (err){ 
        let obj = {
          err: err
          , connection: req.mysqlDb
        };
        return reject( obj );
      }

      if(rows !== undefined){
        return rows.changedRows || rows.affectedRows || rows.insertId ? resolve(true) : resolve(rows);
      }

    });

    req.mysqlDb.end( ( err ) => { if(err){ console.log("End method -- an error has occurred..." , err ) } else { console.log("The connection is terminated now." ); } } );
    
  } );

};

module.exports = {queryDB};
// https://graphql.org/code/#javascript
// https://www.npmjs.com/package/express-graphql